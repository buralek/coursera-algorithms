import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {
    private WeightedQuickUnionUF grid;
    private int topFake;
    private int bottomFake;
    private int n;
    private boolean[] openedSites;
    private int numberOfOpenSites;

    // creates n-by-n grid, with all sites initially blocked
    public Percolation(int n) {
        if (n > 0) {
            grid = new WeightedQuickUnionUF(n * n + 2);
            openedSites = new boolean[n * n + 2];
            this.n = n;
            topFake = 0;
            bottomFake = n * n +1;
            numberOfOpenSites = 0;
        } else {
            throw new IllegalArgumentException("N can't be low than 1");
        }
    }

    // opens the site (row, col) if it is not open already
    public void open(int row, int col) {
        if (isOpen(row, col)) {
            return;
        }

        int gridNumber = getGridNumber(row, col);
        openedSites[gridNumber] = true;
        numberOfOpenSites++;

        if (row == 1) {
            grid.union(topFake, gridNumber);
        }
        if (row == n) {
            grid.union(bottomFake, gridNumber);
        }

        // check top
        if (isOpen(row - 1, col)) {
            grid.union(gridNumber, getGridNumber(row - 1, col));
        }
        // check right
        if (isOpen(row, col + 1)) {
            grid.union(gridNumber, getGridNumber(row, col + 1));
        }
        // check bottom
        if (isOpen(row + 1, col)) {
            grid.union(gridNumber, getGridNumber(row + 1, col));
        }
        // check left
        if (isOpen(row, col - 1)) {
            grid.union(gridNumber, getGridNumber(row, col - 1));
        }
    }

    // is the site (row, col) open?
    public boolean isOpen(int row, int col) {
        if (row <= 0 || col <= 0) {
            return false;
        }
        if (row > n || col > n) {
            return false;
        }
        int gridNumber = getGridNumber(row, col);
        return openedSites[gridNumber];
    }

    // is the site (row, col) full?
    public boolean isFull(int row, int col) {
        int siteFind = grid.find(getGridNumber(row, col));
        int topFakeFind = grid.find(topFake);
        return siteFind == topFakeFind;
    }

    // returns the number of open sites
    public int numberOfOpenSites() {
        return numberOfOpenSites;
    }

    // does the system percolate?
    public boolean percolates() {
        int topFakeFind =  grid.find(topFake);
        int bottomFakeFind = grid.find(bottomFake);
        return topFakeFind == bottomFakeFind;
    }

    // does the system percolate?
    /*public boolean percolates() {
        boolean isPerlocated = false;
        int topFakeFind =  grid.find(topFake);
        int col = 1;
        while (!isPerlocated && col <= n) {
            int colFind = grid.find(getGridNumber(n, col));
            isPerlocated = (topFakeFind == colFind);
            col++;
        }

        return isPerlocated;
    }*/

    private int getGridNumber(int row, int col) {
        if (row <= 0 || col <= 0) {
            throw new IllegalArgumentException("Row or Col can't be less than 1");
        }
        if (row > n || col > n) {
            throw new IllegalArgumentException("Row or Col can't be more than " + n);
        }
        return (row - 1) * n + col;
    }
}

