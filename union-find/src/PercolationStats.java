import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {
    private final int trials;
    private final double[] fractions;
    private final static double CONFIDENCE_95 = 1.96;
    private double mean = -1;

    // perform independent trials on an n-by-n grid
    public PercolationStats(int n, int trials) {
        this.trials = trials;
        fractions = new double[trials];
        for (int t = 0; t < trials; t++) {
            int step = 0;
            // calculate step
            Percolation percolation = new Percolation(n);
            while (!percolation.percolates()) {
                int nextOpenedSiteRow = StdRandom.uniform(1, n + 1);
                int nextOpenedSiteCol = StdRandom.uniform(1, n + 1);
                if (!percolation.isOpen(nextOpenedSiteRow, nextOpenedSiteCol)) {
                    percolation.open(nextOpenedSiteRow, nextOpenedSiteCol);
                    step++;
                }
            }
            fractions[t] = (double) step / (n*n);
        }
    }

    // sample mean of percolation threshold
    public double mean() {
        mean = StdStats.mean(fractions);
        return mean;
    }

    // sample standard deviation of percolation threshold
    public double stddev() {
        return StdStats.stddev(fractions);
    }

    // low endpoint of 95% confidence interval
    public double confidenceLo() {
        if (mean != -1) {
            return mean - ((CONFIDENCE_95 * stddev()) / Math.sqrt(trials));
        } else {
            return mean() - ((CONFIDENCE_95 * stddev()) / Math.sqrt(trials));
        }
    }

    // high endpoint of 95% confidence interval
    public double confidenceHi() {
        if (mean != -1) {
            return mean + ((CONFIDENCE_95 * stddev()) / Math.sqrt(trials));
        } else {
            return mean() + ((CONFIDENCE_95 * stddev()) / Math.sqrt(trials));
        }
    }

    // test client (see below)
    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        int trials = Integer.parseInt(args[1]);
        PercolationStats stats = new PercolationStats(n, trials);
        StdOut.println("mean = " + stats.mean());
        StdOut.println("stddev = " + stats.stddev());
        StdOut.println("95% confidence interval = [" + stats.confidenceLo() + ", " + stats.confidenceHi() + "]");
    }
}
