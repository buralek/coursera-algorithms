import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

import java.util.LinkedList;

public class Board {
    private static final int SPACE = 0;
    private int[][] tiles;
    // create a board from an n-by-n array of tiles,
    // where tiles[row][col] = tile at (row, col)
    public Board(int[][] tiles) {
        this.tiles = copy(tiles);
    }

    // string representation of this board
    public String toString() {
        String result = new String();
        result += tiles.length + "\n";
        for (int row = 0; row < tiles.length; row++) {
            for (int col = 0; col < tiles.length; col++) {
                result += " " + tiles[row][col];
            }
            result += "\n";
        }
        return result;
    }

    // board dimension n
    public int dimension() {
        return tiles.length;
    }

    // number of tiles out of place
    public int hamming() {
        int hammingCounter = 0;
        for (int row = 0; row < tiles.length; row++) {
            for (int col = 0; col < tiles.length; col++) {
                if (!isTileOk(row, col)) {
                    hammingCounter++;
                }
            }
        }
        return hammingCounter;
    }

    // sum of Manhattan distances between tiles and goal
    public int manhattan() {
        int manhattanCounter = 0;
        for (int row = 0; row < tiles.length; row++) {
            for (int col = 0; col < tiles.length; col++) {
                int correctRow = correctRow(tiles[row][col]);
                int correctCol = correctCol(tiles[row][col]);
                int rowDif = Math.abs(correctRow - row);
                int colDif = Math.abs(correctCol - col);
                manhattanCounter += (rowDif + colDif);
            }
        }
        return manhattanCounter;
    }

    // is this board the goal board?
    public boolean isGoal() {
        return hamming() == 1;
    }

    // does this board equal y?
    public boolean equals(Object y) {
        Board newBoard = (Board) y;
        int[][] newTiles = newBoard.tiles;
        if (newTiles.length != tiles.length) {
            return false;
        }
        for (int row = 0; row < tiles.length; row++) {
            for (int col = 0; col < tiles.length; col++) {
                if (tiles[row][col] != newTiles[row][col]) {
                    return false;
                }
            }
        }
        return true;
    }

    // all neighboring boards
    public Iterable<Board> neighbors() {
        LinkedList<Board> neighbors = new LinkedList<>();

        int[] location = spaceLocation();
        int spaceRow = location[0];
        int spaceCol = location[1];

        if (spaceRow > 0) {
            neighbors.add(new Board(swap(spaceRow, spaceCol, spaceRow - 1, spaceCol)));
        }
        if (spaceRow < dimension() - 1) {
            neighbors.add(new Board(swap(spaceRow, spaceCol, spaceRow + 1, spaceCol)));
        }
        if (spaceCol > 0) {
            neighbors.add(new Board(swap(spaceRow, spaceCol, spaceRow, spaceCol - 1)));
        }
        if (spaceCol < dimension() - 1) {
            neighbors.add(new Board(swap(spaceRow, spaceCol, spaceRow, spaceCol + 1)));
        }

        return neighbors;
    }

    // a board that is obtained by exchanging any pair of tiles
    public Board twin() {
        for (int row = 0; row < tiles.length; row++) {
            for (int col = 0; col < tiles.length - 1; col++) {
                if (tiles[row][col] != SPACE && tiles[row][col+1] != SPACE) {
                    return new Board(swap(row, col, row, col+1));
                }
            }
        }
        throw new RuntimeException("Can't find a twin");
    }

    private int[][] copy(int[][] tiles) {
        int[][] copy = new int[tiles.length][tiles.length];
        for (int row = 0; row < tiles.length; row++)
            for (int col = 0; col < tiles.length; col++)
                copy[row][col] = tiles[row][col];

        return copy;
    }

    private boolean isTileOk(int row, int col) {
        int correctTileValue = row * tiles.length + col +1;
        return correctTileValue == tiles[row][col];
    }

    private int correctRow(int value) {
        return value/tiles.length;
    }

    private int correctCol(int value) {
        int correctRow = correctRow(value);
        return value - correctRow*tiles.length - 1;
    }

    private int[][] swap(int row1, int col1, int row2, int col2) {
        int[][] copyTiles = copy(tiles);
        int value = copyTiles[row1][col1];
        copyTiles[row1][col1] = copyTiles[row2][col2];
        copyTiles[row2][col2] = value;
        return copyTiles;
    }

    private int[] spaceLocation() {
        for (int row = 0; row < tiles.length; row++)
            for (int col = 0; col < tiles.length; col++)
                if (tiles[row][col] == SPACE) {
                    int[] location = new int[2];
                    location[0] = row;
                    location[1] = col;
                    return location;
                }
        throw new RuntimeException();
    }

    // unit testing (not graded)
    public static void main(String[] args) {
        // create initial board from file
        In in = new In(args[0]);
        int n = in.readInt();
        int[][] tiles = new int[n][n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                tiles[i][j] = in.readInt();
        Board board = new Board(tiles);

        StdOut.println("Hamming = " + board.hamming());
        StdOut.println("Manhatten = " + board.manhattan());
    }
}
