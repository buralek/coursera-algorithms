import edu.princeton.cs.algs4.StdOut;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {
    private Node first;
    private Node last;
    private int size;

    // construct an empty deque
    public Deque() {
        first = null;
        last = null;
        size = 0;
    }

    // is the deque empty?
    public boolean isEmpty() {
        return size == 0;
    }

    // return the number of items on the deque
    public int size() {
        return size;
    }

    // add the item to the front
    public void addFirst(Item item) {
        checkNotNull(item);
        // link to old first node
        Node oldFirst = first;

        // create new first node and set old to next
        first = new Node();
        first.item = item;
        first.previous = null;
        first.next = oldFirst;

        // update oldFirst.previous
        if (oldFirst != null) {
            oldFirst.previous = first;
        }

        // update last
        if (size == 0) {
            last = first;
        } else if (size == 1) {
            last.previous = first;
        }

        size++;
    }

    // add the item to the back
    public void addLast(Item item) {
        checkNotNull(item);
        // link to old last node
        Node oldLast = last;

        // create new last node and set old to previous
        last = new Node();
        last.item = item;
        last.previous = oldLast;
        last.next = null;

        // update oldLast.next
        if (oldLast != null) {
            oldLast.next = last;
        }

        // update first
        if (size == 0) {
            first = last;
        } else if (size == 1) {
            first.next = last;
        }
        size++;
    }

    // remove and return the item from the front
    public Item removeFirst() {
        checkNotEmpty();
        Node oldFirst = copyNode(first);
        first = oldFirst.next;
        if (first != null) {
            first.previous = null;
        }
        return oldFirst.item;
    }

    // remove and return the item from the back
    public Item removeLast() {
        checkNotEmpty();
        Node oldLast = copyNode(last);
        last = oldLast.previous;
        if (last != null) {
            last.next = null;
        }
        return oldLast.item;
    }

    // return an iterator over items in order from front to back
    public Iterator<Item> iterator() {
        return new DequeIterator();
    }

    private void checkNotNull(Item item) {
        if (item == null) {
            throw new IllegalArgumentException("Item can't be null");
        }
    }

    private void checkNotEmpty() {
        if (size == 0) {
            throw new NoSuchElementException("Deque is empty");
        }
    }

    private Node copyNode(Node node) {
        Node copiedNode = null;
        if (node != null) {
            copiedNode = new Node();
            copiedNode.item = node.item;
            copiedNode.next = node.next;
            copiedNode.previous = node.previous;
        }
        return copiedNode;
    }

    private class Node {
        private Item item;
        private Node next;
        private Node previous;
    }

    private class DequeIterator implements Iterator<Item> {
        private Node current = first;

        public boolean hasNext()  { return current != null;                     }
        public void remove()      { throw new UnsupportedOperationException();  }

        public Item next() {
            if (!hasNext()) throw new NoSuchElementException();
            Item item = current.item;
            current = current.next;
            return item;
        }
    }

    // unit testing (required)
    public static void main(String[] args) {
        Deque<String> deque = new Deque<String>();
        for (String item : args) {
            if (item.equals("-")) {
                StdOut.println("Remove first element " + deque.removeFirst());
            } else if (item.equals("+")) {
                StdOut.println("Remove last element " + deque.removeLast());
            } else if (item.startsWith("-")) {
                StdOut.println("Add first element " + item.substring(1));
                deque.addFirst(item.substring(1));
            } else if (item.startsWith("+")) {
                StdOut.println("Add last element " + item.substring(1));
                deque.addLast(item.substring(1));
            }
        }
        StdOut.println("Deque size = " + deque.size());
        Iterator<String> dequeIterator = deque.iterator();
        StdOut.println("Deque elements:");
        while (dequeIterator.hasNext()) {
            StdOut.println(dequeIterator.next());
        }
    }

}
