import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class RandomizedQueue<Item> implements Iterable<Item> {
    private final static int START_SIZE = 2;
    private Item[] queue;
    private int n;

    // construct an empty randomized queue
    public RandomizedQueue() {
        queue = (Item[]) new Object[START_SIZE];
        n = 0;
    }

    // is the randomized queue empty?
    public boolean isEmpty() {
        return n == 0;
    }

    // return the number of items on the randomized queue
    public int size() {
        return n;
    }

    // add the item
    public void enqueue(Item item) {
        checkNotNull(item);
        if (n == queue.length) {
            resize(2 * queue.length);
        }
        queue[n] = item;
        n++;
    }

    // remove and return a random item
    public Item dequeue() {
        checkNotEmpty();
        int randomIndex = StdRandom.uniform(0, n);
        Item item = queue[randomIndex];
        queue[randomIndex] = queue[n-1];
        queue[n-1] = null;
        n--;
        shrink();
        return item;
    }

    // return a random item (but do not remove it)
    public Item sample() {
        checkNotEmpty();
        int randomIndex = getRandomIndex();
        return queue[randomIndex];
    }

    // return an independent iterator over items in random order
    public Iterator<Item> iterator() {
        return new RandomQueueIterator();
    }

    private void checkNotNull(Item item) {
        if (item == null) {
            throw new IllegalArgumentException("Item can't be null");
        }
    }

    private void checkNotEmpty() {
        if (n == 0) {
            throw new NoSuchElementException("Deque is empty");
        }
    }

    private void resize(int capacity) {
        Item[] copy = (Item[]) new Object[capacity];
        for (int i = 0; i < n; i++) {
            copy[i] = queue[i];
        }
        queue = copy;
    }

    private void shrink() {
        if (n > 0 && n == queue.length/4) {
            resize(queue.length/2);
        }
    }

    private int getRandomIndex() {
        return StdRandom.uniform(0, n);
    }

    private class RandomQueueIterator implements Iterator<Item> {
        private int i = 0;
        private Item[] randomQueue;

        public RandomQueueIterator() {
            copyQueue();
            StdRandom.shuffle(randomQueue);
        }

        @Override
        public boolean hasNext() {
            return i < n;
        }

        @Override
        public Item next() {
            if (!hasNext()) throw new NoSuchElementException();
            Item item = queue[i];
            i++;
            return item;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }

        private void copyQueue() {
            randomQueue = (Item[]) new Object[n];
            for (int i = 0; i < n; i++) {
                randomQueue[i] = queue[i];
            }
        }

    }
    // unit testing (required)
    public static void main(String[] args) {
        RandomizedQueue<String> rQueue = new RandomizedQueue<>();
        for (String item : args) {
            if (item.equals("-")) {
                StdOut.println("Remove random element " + rQueue.dequeue());
            } else {
                StdOut.println("Add element " + item);
                rQueue.enqueue(item);
            }
        }
        StdOut.println("rQueue size = " + rQueue.size());
        StdOut.println("rQueue random element = " + rQueue.sample());
        Iterator<String> rQueueIterator = rQueue.iterator();
        StdOut.println("rQueue elements:");
        while (rQueueIterator.hasNext()) {
            StdOut.println(rQueueIterator.next());
        }
    }

}
