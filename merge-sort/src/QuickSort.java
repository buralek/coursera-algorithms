import java.util.Random;

public class QuickSort {
    public static void main(String[] args) {
        //int[] actual = { 5, 1, 6, 2, 3, 4 };
        int[] actual = { 1, 6, 5, 3, 2, 4 };
        quickSort(actual);
        for (int i = 0; i < actual.length; i++) {
            System.out.print(actual[i] + ", ");
        }
    }

    public static void quickSort(int[] array) {
        //shuffle(array);
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + ", ");
        }
        System.out.println();
        sort(array, 0, array.length-1);
    }

    private static void shuffle(int[] array) {
        Random rand = new Random();
        for (int i = 0; i < array.length; i++) {
            int randomIndexToSwap = rand.nextInt(array.length);
            int temp = array[randomIndexToSwap];
            array[randomIndexToSwap] = array[i];
            array[i] = temp;
        }
    }

    private static void sort(int[] array, int low, int high) {
        if (low >= high) {
            return;
        }
        int j = partition(array, low, high);
        sort(array, low, j-1);
        sort(array, j+1, high);
    }

    private static int partition(int[] array, int low, int high) {
        int i = low +1;
        int j = high;
        while (i <= j) {
            while (i <=high && array[i] < array[low]) {
                i++;
            }
            while (j >=low && array[j] > array[low]) {
                j--;
            }
            if (i < j) {
                swap(array, i, j);
            }
        }
        swap(array, low, j);
        return j;
    }

    private static void swap(int[] array, int one, int two) {
        int swap = array[one];
        array[one] = array[two];
        array[two] = swap;
    }

}
