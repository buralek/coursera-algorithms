public class MergeSort {
    public static void main(String[] args) {
        int[] actual = { 5, 1, 6, 2, 3, 4 };
        mergeSort(actual);
        for (int i = 0; i < actual.length; i++) {
            System.out.print(actual[i] + ", ");
        }
    }

    public static void mergeSort(int[] array) {
        int size = array.length;
        if (size < 2) {
            return;
        }
        int mid = size/2;
        int[] left = new int[mid];
        int[] right = new int[size - mid];

        for (int i = 0; i < mid; i++) {
            left[i] = array[i];
        }
        for (int i = mid; i < size; i++) {
            right[i - mid] = array[i];
        }

        mergeSort(left);
        mergeSort(right);

        merge(array, left, right, mid, size - mid);
    }

    public static void merge(int[] array, int[] left, int[] right, int leftMax, int rightMax) {
        int arrayIndex = 0;
        int leftIndex = 0;
        int rightIndex = 0;
        while (leftIndex < leftMax && rightIndex < rightMax) {
            if (left[leftIndex] <= right[rightIndex]) {
                array[arrayIndex] = left[leftIndex];
                leftIndex++;
            } else {
                array[arrayIndex] = right[rightIndex];
                rightIndex++;
            }
            arrayIndex++;
        }

        while (leftIndex < leftMax) {
            array[arrayIndex] = left[leftIndex];
            arrayIndex++;
            leftIndex++;
        }

        while (rightIndex < rightMax) {
            array[arrayIndex] = right[rightIndex];
            arrayIndex++;
            rightIndex++;
        }
    }
}
